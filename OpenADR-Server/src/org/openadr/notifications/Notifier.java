package org.openadr.notifications;

public interface Notifier {

	public void sendNotification(Notification notification) ;
	
}
