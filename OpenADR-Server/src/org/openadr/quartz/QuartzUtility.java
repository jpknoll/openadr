package org.openadr.quartz;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.openadr.util.*;

public class QuartzUtility {
	private static final Log log = LogFactory.getLog(QuartzUtility.class);
	
	private static Scheduler sched = null;
	private static StdSchedulerFactory schedFact = null;
	
	
	public static void startQuartz() {
		try{
			if(schedFact == null){
				Properties props = ConfigurationLoader.loadProps("quartz.properties");
				schedFact = new org.quartz.impl.StdSchedulerFactory();
				schedFact.initialize(props);
			}
			if(sched == null){
				sched = schedFact.getScheduler();
				sched.start();
			}
		}catch(IOException e){
			log.error("IOException: " + e.getMessage(), e);
		}catch(SchedulerException e){
			log.error("SchedulerException: " + e.getMessage(), e);
		}		
	}
	
	public static void stopQuartz() {
		if(sched != null)
			try {
				sched.shutdown();
				sched = null;
			} catch (SchedulerException e) {
				log.error("SchedulerException: " + e.getMessage(), e);
			}
	}
	
	public static synchronized void scheduleJob(JobDetail job, SimpleTrigger trigger) {
		if(sched == null)
			startQuartz();
		try{
			sched.scheduleJob(job, trigger);
		}catch(SchedulerException e){
			log.error("SchedulerException: " + e.getMessage(), e);
		}
	}
	
	public static synchronized void scheduleJob(String jobName, Class jobClass, Date jobDate, Map params) {
		try{
			JobDetail jobDetail = new JobDetail(jobName, Scheduler.DEFAULT_GROUP, jobClass);
			jobDetail.getJobDataMap().putAll(params);
   		
			SimpleTrigger trigger = new SimpleTrigger(jobName, Scheduler.DEFAULT_GROUP,
					jobDate, null, 0, 0L);
			if(sched == null)
				startQuartz();
				
			sched.scheduleJob(jobDetail, trigger);
		}catch(SchedulerException e){
			log.error("SchedulerException: " + e.getMessage(), e);
		}
	}

	public static synchronized void deleteJob(String jobName) {
		if(sched == null)
			startQuartz();
		try{	
			sched.deleteJob(jobName, Scheduler.DEFAULT_GROUP);
		}catch(SchedulerException e){
			log.error("SchedulerException: " + e.getMessage(), e);
		}
	}
}
