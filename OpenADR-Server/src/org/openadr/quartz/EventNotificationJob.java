package org.openadr.quartz;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class EventNotificationJob implements Job {
	private static Log log = LogFactory.getLog(EventNotificationJob.class);
	
	public void execute(JobExecutionContext jobCtx)  throws JobExecutionException {
		try {
			executeEventNotification(jobCtx);
		} catch (Exception e) {
			log.error("ServiceException: " + e.getMessage(), e);
		}
		
    }

	private void executeEventNotification(JobExecutionContext jobCtx) throws Exception {
		String type = (String)jobCtx.getJobDetail().getJobDataMap().get("");
		
		if(type.equals("")){
			sendNotification(jobCtx);
			return;
		}

		log.error("The Remainder Job has not been executed: ");
		JobDetail detail = jobCtx.getJobDetail();
		if(detail != null){
			log.error("Job Detail :");						
			log.error(detail.getDescription());						
			log.error(detail.getFullName());						
			log.error(detail.getJobDataMap());						
		} else {
			log.error("Job Detail = null");			
		}
	}
	
	private void sendNotification(JobExecutionContext jobCtx)  {
	}

}
