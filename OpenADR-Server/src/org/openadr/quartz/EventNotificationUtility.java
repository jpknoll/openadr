package org.openadr.quartz;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.openadr.config.Constants;
import org.openadr.model.event.UtilityDREvent;
import org.openadr.model.participant.ContactInformation;
import org.openadr.model.participant.ObjectFactory;
import org.openadr.model.participant.ParticipantAccount;


public class EventNotificationUtility {
	private static Log log = LogFactory.getLog(EventNotificationUtility.class);	

	private static void scheduleEventNotification(ParticipantAccount participant,  
			UtilityDREvent event) {
		try{	        
	        Map map = new HashMap();
			map.put(Constants.emailAddress, participant.getContactInformation().getValue().getEmailAddresses());
	    	map.put(Constants.eventText, "Test Message");
			Calendar dueCal = Calendar.getInstance();
			dueCal.clear();
			Date dueDate = event.getEventTiming().getNotificationTime().toGregorianCalendar().getTime();
			dueCal.setTime(dueDate);
	    	//dueCal.set(Calendar.HOUR_OF_DAY, 0);
	        //dueCal.set(Calendar.MINUTE, 1);
	        
        	QuartzUtility.scheduleJob("EventNotification", EventNotificationJob.class, dueCal.getTime(), map);
		}catch(Exception e){
			log.error("Exception setting Offer Response Reminder: " + e.getMessage(), e);
		}
	}

	public static void main(String args[]) {
		ParticipantAccount participant = new ParticipantAccount();
		participant.setAccountID("id123");
		ContactInformation ci = new ContactInformation();		
		ci.setEmailAddresses(new ObjectFactory().createContactInformationEmailAddresses(new ContactInformation.EmailAddresses()));
		ci.getEmailAddresses().getValue().getAddress().add("sraju@uisol.com");
		participant.setContactInformation(new ObjectFactory().createParticipantAccountContactInformation(ci));

		UtilityDREvent event = new UtilityDREvent();
		XMLGregorianCalendar gc = null;//new XMLGregorianCalendarImpl();
		gc.setYear(2009);
		gc.setMonth(11);
		gc.setDay(31);
		gc.setTime(01, 01, 01, 01);
		event.setEventTiming(new UtilityDREvent.EventTiming());
		event.getEventTiming().setNotificationTime(gc);
		
		EventNotificationUtility.scheduleEventNotification(participant, event);
	}
}
