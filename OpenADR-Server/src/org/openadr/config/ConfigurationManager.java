package org.openadr.config;

import java.io.IOException;
import java.util.Properties;

public class ConfigurationManager extends Properties {
	private static ConfigurationManager manager = null;
	
	public ConfigurationManager() throws Exception {
		readConfig();
	}
	
	public ConfigurationManager getInstance() throws Exception {
		if ( manager == null ) {
			manager = new ConfigurationManager();
		}
		
		return manager;
	}
	
	private void readConfig() throws Exception {
		load(this.getClass().getResourceAsStream("config.properties"));
	}

	public boolean sendEmail() {
		String flag = getProperty("sendEmail", "false");
		
		return (new Boolean(flag).booleanValue());
	}
	
	public String getEmailHost() {
		return getProperty("emailHost", "localhost");
	}
	
	public String getEmailFrom() {
		return getProperty("emailFrom", "openadr@openadr.com");
	}
}
