package org.openadr.util;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

// Send a simple, single part, text/html e-mail
public class Mailer {
      Properties props = new Properties();
      Session session;
      Transport transport;
      
    
    public void sendMail(ArrayList<String> toAddress, 
            String fromAddress,
            String subject, 
            String content) throws MessagingException{

      // Instantiate a message
            Message msg = new MimeMessage(session);

            //Set message attributes
            msg.setFrom(new InternetAddress(fromAddress));
            
            InternetAddress[] address = new InternetAddress[toAddress.size()];
            
            for ( int i = 0; i < toAddress.size(); i++ ) {
                  address[i] = new InternetAddress(toAddress.get(i));
            }
            
            msg.setRecipients(Message.RecipientType.TO, address);            
            msg.setSubject(subject);
            msg.setSentDate(new Date());

            msg.setContent(content, "text/html");
            msg.saveChanges(); 
            transport.sendMessage(msg, msg.getAllRecipients());
            //transport.send(msg);
      
    }
    
    public Mailer(String emailHost, String emailUser, String emailUserPassword) throws Exception {
        props.put("mail.smtp.host", emailHost);
        //props.put("mail.debug", "true");
        
        if ( emailUser != null )
            props.put("mail.smtp.user", emailUser);
        
        if ( emailUserPassword != null )
            props.put("mail.smtp.password", emailUserPassword);

        session = Session.getInstance(props, null);
        transport = session.getTransport("smtp");
        transport.connect(emailHost, null, null);

    }
    
    public void close() throws Exception {
      transport.close();
    }
    
}
