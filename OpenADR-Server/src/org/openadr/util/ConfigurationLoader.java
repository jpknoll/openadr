package org.openadr.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class ConfigurationLoader {

	private static String PROP_DIR = "properties";

	
	public static Properties loadProps( String propName )
		throws FileNotFoundException,
			   IOException{
        Properties props;
        try {
    		FileInputStream fin = new FileInputStream( PROP_DIR + File.separator + propName );
    		props = new Properties();
    		props.load( fin );
        } catch (FileNotFoundException e) {
        	String propertyPath =  "/" + PROP_DIR + "/" +  propName;
            InputStream is = ConfigurationLoader.class.getResourceAsStream(propertyPath);
            if (is == null) {
                throw new FileNotFoundException(propName);
            }
            props = new Properties();
            props.load(is);
        }
		return props;
	}

	public static Properties loadProps( String propPath, String propName )
	throws FileNotFoundException,
		   IOException{
    Properties props;
    try {
    	StringBuffer propertyPath = new StringBuffer();
    	if(propPath != null){
    		propertyPath.append(propPath).append(File.separator);
    	} 
    	propertyPath.append(propName);
		FileInputStream fin = new FileInputStream( propertyPath.toString() );
		props = new Properties();
		props.load( fin );
    } catch (FileNotFoundException e) {
    	StringBuffer propertyPath = new StringBuffer();
    	if(propPath != null){
    		propertyPath.append("/").append(propPath);
    	}
		propertyPath.append("/").append(propName);
        InputStream is = ConfigurationLoader.class.getResourceAsStream(propertyPath.toString());
        if (is == null) {
            throw new FileNotFoundException(propName);
        }
        props = new Properties();
        props.load(is);
    }
	return props;
}
	
	public static String loadFile( String propName )
	throws FileNotFoundException,
		   IOException{
    
    try {
		FileInputStream fin = new FileInputStream( PROP_DIR + File.separator + propName );
		if(fin != null){
			return getFromFile(fin);
		}
    } catch (FileNotFoundException e) {
    	String propertyPath =   "/" + PROP_DIR + "/" +  propName;
        InputStream is = ConfigurationLoader.class.getResourceAsStream(propertyPath);
        if (is == null) {
            throw new FileNotFoundException(propName);
        }
        if(is != null){
        	return getFromFile(is);
        }
    }
    throw new FileNotFoundException("Could not get the resource " + propName);
}

	public static String loadFile( String path, String propName )
	throws FileNotFoundException,
		   IOException{
    
	    try {
			FileInputStream fin = new FileInputStream( path + File.separator + propName );
			if(fin != null){
				return getFromFile(fin);
			}
	    } catch (FileNotFoundException e) {
	    	StringBuffer propertyPath = new StringBuffer();
	    	if(path != null){
	    		propertyPath.append("/").append(path);
	    	}
			propertyPath.append("/").append(propName);
	        InputStream is = ConfigurationLoader.class.getResourceAsStream(propertyPath.toString());
	        if (is == null) {
	            throw new FileNotFoundException(propName);
	        }
	        if(is != null){
	        	return getFromFile(is);
	        }
	    }
	    throw new FileNotFoundException("Could not get the resource " + propName);
	}
	
	  
    public static String getFromFile(File xmlFile) throws IOException {
    	FileInputStream fis = new FileInputStream(xmlFile);
    	return readFromStream(fis);
    }

    private static String getFromFile(InputStream stream) throws IOException {
        return readFromStream(stream);
    }
    
    private static String readFromStream(InputStream stream )throws IOException{
    	StringBuffer sbuf = new StringBuffer();
    	BufferedReader bufReader = new BufferedReader(new InputStreamReader(stream));
        int c;
        while ((c = bufReader.read()) !=  -1){
        	sbuf.append((char)c);
        }       
        return sbuf.toString();
    }

	public static File getFile(String pathname, String filename) {
		String actualPath = (pathname == null) ? PROP_DIR : pathname;
		File f = new File(actualPath + File.separator + filename);
		return f;
	}
    
    
}
