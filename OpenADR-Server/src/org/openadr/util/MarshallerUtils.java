package org.openadr.util;

import java.io.FileWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.openadr.model.program.UtilityProgram;

public class MarshallerUtils {

	public static void printProgram(UtilityProgram program) {
		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance("org.openadr.model.program");	
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			//m.marshal(program, System.out);
			m.marshal(program, new FileWriter("c:\\temp\\test.xml"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
