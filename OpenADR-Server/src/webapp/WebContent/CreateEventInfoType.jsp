<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
  import = "java.io.*"
  import = "java.util.*"
%>
<HTML>
<HEAD><TITLE>Create Event Info Type:</TITLE></HEAD>
<H1>Create Event Info Type:</H1>
<BODY>
<FORM METHOD=POST ACTION="SaveEventInfoType.jsp">
<Table border="0">
<tr align="left" valign="top">
<td>Associated program name:</td>
<td><%= session.getAttribute("programName")%></td>
</tr>
<tr align="left" valign="top">
<td>Event Info Type Name:</td>
<td><INPUT TYPE=TEXT NAME=typeName SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td>Event Info Type ID:</td>
<td><INPUT type="radio" name="typeID" value="PRICE_ABSOLUTE"> PRICE_ABSOLUTE<BR>
    <INPUT type="radio" name="typeID" value="PRICE_RELATIVE"> PRICE_RELATIVE<BR>
    <INPUT type="radio" name="typeID" value="PRICE_MULTIPLE"> PRICE_MULTIPLE<BR>
</td>
</tr>
<tr align="left" valign="top">
<td>Schedule type:</td>
<td><INPUT type="radio" name="scheduleType" value="None"> None<BR>
    <INPUT type="radio" name="scheduleType" value="Dynamic"> Dynamic<BR>
    <INPUT type="radio" name="scheduleType" value="Static"> Static<BR>
</td>
</tr>
<tr align="left" valign="top">
<td><b>OPTIONAL</b>Schedule time slots (comma separated list):</td> 
<td><INPUT TYPE=TEXT NAME=scheduleTimeslots SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td><b>OPTIONAL</b> Possible event info values (comma separated list):</td> 
<td><INPUT TYPE=TEXT NAME=eventInfoValues SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td>Minimum event info value:</td>
<td><INPUT TYPE=TEXT NAME=minValue SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td>Maximum event info value:</td>
<td><INPUT TYPE=TEXT NAME=maxValue SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td><input type="submit" name="submitEIT" value="Add Another EventInfoType"/></td>
<td><input type="submit" name="submitEIT" value="Finish"/></td>
</tr>
</Table>
</FORM>
</BODY>
</HTML>