<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
  import = "java.io.*"
  import = "java.sql.*"
  import = "java.util.*"
  import = "javax.sql.*"
%>
<%
  Connection con = null;  
  ResultSet rs = null;
  Statement stmt = null;
%>
<HTML>
<HEAD><TITLE>Create DRAS Client:</TITLE></HEAD>
<H1>Create DRAS Client:</H1>
<BODY>
<FORM METHOD=POST ACTION="SaveDRASClient.jsp">
<Table border="0"><tr align="left" valign="top">
<tr align="left" valign="top">
<td>Account ID:</td>
<td><%= session.getAttribute("accountID")%></td>
</tr>
<tr align="left" valign="top">
<td>Client identifier:</td>
<td><INPUT TYPE=TEXT NAME=identifier SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td>Client type:</td>
<td>SMART</td>
</tr>
<tr align="left" valign="top">
<td>Online status:</td>
<td><INPUT type="radio" name="onLineStatus" value="Online"> Online<BR>
    <INPUT type="radio" name="onLineStatus" value="Offline"> Offline<BR></td>
</tr>
<tr align="left" valign="top">
<td>Connection type:</td>
<td>PULL</td>
</tr>
<tr align="left" valign="top">
<td>Client URI:</td>
<td><i>N/A (only used for PUSH clients)</i></td>
</tr>
<tr align="left" valign="top">
<td>Client authentication:</td> 
<td><i>N/A (only used for PUSH clients)</i></td>
</tr>
<tr align="left" valign="top">
<td>Polling period:</td> 
<td><INPUT TYPE=TEXT NAME=pollingPeriod SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td>Retry count:</td> 
<td><i>N/A (only used for PUSH clients)</i></td>
</tr>
<tr align="left" valign="top">
<td>Retry period:</td>
<td><i>N/A (only used for PUSH clients)</i></td>
</tr>
<tr align="left" valign="top">
<td>Grid location:</td> 
<td><INPUT TYPE=TEXT NAME=gridLocation SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td>Latitude:</td> 
<td><INPUT TYPE=TEXT NAME=latitude SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td>longitude:</td> 
<td><INPUT TYPE=TEXT NAME=longitude SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td>Address:</td> 
<td><INPUT TYPE=TEXT NAME=address SIZE=40></td>
</tr>
<tr align="left" valign="top">
<td><input type="submit" name="submit" value="Submit"/></td>
</tr>
</Table>
</FORM>
</BODY>
</HTML>