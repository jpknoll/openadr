<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
  import = "java.io.*"
  import = "java.util.*"
  import = "org.openadr.dataobjects.participant.ParticipantAccount"
  import = "org.openadr.dataobjects.participant.Contact"
  import = "org.openadr.dataobjects.logging.*"
  import = "org.openadr.*"
%>
<%
  String name = request.getParameter("name");
  session.setAttribute("name", name);
  
  String accountID = request.getParameter("accountID");
  session.setAttribute("accountID", accountID);
  
  String username = request.getParameter("username");
  String password = request.getParameter("password");
  String emails = request.getParameter("emails");
  String voicenumbers = request.getParameter("voicenumbers");
  String pagernumbers = request.getParameter("pagernumbers");
  String faxnumbers = request.getParameter("faxnumbers");
  String [] emailTokens = null;
  String [] voiceTokens = null;
  String [] pagerTokens = null;
  String [] faxTokens = null;
  int numEmails = -1;
  int numVoices = -1;
  int numPagers = -1;
  int numFaxes = -1;
  int numContacts;
%>
<html>
<head>
  <title>Inserting Participant</title>
</head>
<body>
<%
  try {
	ParticipantAccount participant = new ParticipantAccount();
	participant.setParticipantName(name);
	participant.setAccountID(accountID);
	participant.setUserName(username);
	participant.setPassword(password);

	if (emails != null) {
	    emailTokens = emails.split(",");
	    numEmails = emailTokens.length;
	}
	if (voicenumbers != null) {
	    voiceTokens = voicenumbers.split(",");
	    numVoices = voiceTokens.length;
	}
	if (pagernumbers != null) {
	    pagerTokens = pagernumbers.split(",");
	    numPagers = pagerTokens.length;
	}
	if (faxnumbers != null) {
	    faxTokens = faxnumbers.split(",");
	    numFaxes = faxTokens.length;
	}
	
	numContacts = Math.max(Math.max(numEmails, numVoices), 
			      Math.max(numPagers, numFaxes));
	
	for (int i = 0; i < numContacts; i++) {
		Contact contact = new Contact();
		contact.setAccountID(accountID);
		if (i < numEmails)
			contact.setEmailAddress(emailTokens[i]);
		if (i < numFaxes)
			contact.setFaxNumber(faxTokens[i]);
		if (i < numPagers)
			contact.setPagerNumber(pagerTokens[i]);
		if (i < numVoices)
			contact.setPhoneNumber(voiceTokens[i]);
		participant.getContactInformation().add(contact);
    }
	
	DataAccessObject.createObject(participant);
	
	TransactionLog tLog = new TransactionLog();
	tLog.setUserName(username);
	tLog.setRole("Program Administrator");
	tLog.setDescription("participant account created");
	tLog.setResult("SUCCESS");
	tLog.setTimeStamp(new java.util.Date(System.currentTimeMillis()));
	tLog.setTransactionName("CreateParticipantAccount");
	DataAccessObject.createObject(tLog);
	session.setAttribute("createEntity", "ParticipantAccount");
  } catch (Exception e) {
	System.out.println("Incorrect form input: " + e);
	e.printStackTrace();
	%>
	<jsp:forward page="CreateParticipantAccount.html"/>
	<%
  } 
%>
<jsp:forward page="ChoosePrograms.jsp"/>
</body>
</html>