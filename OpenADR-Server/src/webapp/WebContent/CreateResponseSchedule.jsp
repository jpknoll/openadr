<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
  import = "java.io.*"
  import = "java.sql.*"
  import = "java.util.*"
  import = "javax.sql.*"
%>
<%! String[] programs; %>
<%! Integer drasClientId; %>
<%
  Connection conn = null;  
  ResultSet rs = null;
  Statement stmt = null;

  programs = request.getParameterValues("programs");
  int result = -1;
  
  try {
      Class c = Class.forName("oracle.jdbc.driver.OracleDriver");
  } catch (Exception e) {
  	  System.out.println("Error occurred " + e);
  }
  try {
  	  conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "o2adr", "o2adr");
  } catch (SQLException e) {
  	  System.out.println("Error occurred " + e);
  }
  	    
  drasClientId = (Integer) session.getAttribute("drasClientId");
  if (programs != null) {
      String name = (String) session.getAttribute("name");
  	  if (name != null) {
          stmt = conn.createStatement();
  	      result = stmt.executeUpdate("DELETE FROM ProgramDRASClient WHERE drasClientId = " +
  	    		                      drasClientId);
  	  }
      for (int i = 0; i < programs.length; i++) {
          try {
       	      stmt = conn.createStatement();
       	      result = stmt.executeUpdate("INSERT INTO ProgramDRASClient (programDrasClientId, " + 
       	        		                  "                               drasClientId, programId) "+
       	                                  "VALUES(ProgramDRASClient_seq.nextval, " + drasClientId +
       	                                  ", " + programs[i] + ")");
       	  } catch (SQLException e) {
       	      System.out.println("Error occurred " + e);
       	  }
      }
  }
%>
<HTML>
<HEAD><TITLE>Create Response Schedule:</TITLE></HEAD>
<H1>Create Response Schedule:</H1>
<BODY>
<h3>Response schedules are not implemented in this release. Creation/Update of the DRAS client is now complete. DRAS Client ID = <%= drasClientId %></h3>
</BODY>
</HTML>