<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
  import = "org.openadr.*"
  import = "org.openadr.event.*"
  import = "org.openadr.dataobjects.event.*"
  import = "org.openadr.dataobjects.program.*"
%>
<%
  String typeName = request.getParameter("typeName");
  String typeID = null;
  String scheduleType = null;
  String scheduleTimeslots = null;
  String eventInfoValues = null;
  String minValue = null;
  String maxValue = null;
  EventInfoType eventInfoType = null;
%>
<html>
<head>
  <title>Inserting EventInfoType</title>
</head>
<body>
<%
  try {
      if (request.getParameter("typeID") != null) {
          if (request.getParameter("typeID").equals("PRICE_ABSOLUTE")) {
	          typeID = "PRICE_ABSOLUTE";
          } else if (request.getParameter("typeID").equals("PRICE_RELATIVE")) {
	          typeID = "PRICE_RELATIVE";
          } else {
        	  typeID = "PRICE_MULTIPLE";
          }
      }
      if (request.getParameter("scheduleType") != null) {
          if (request.getParameter("scheduleType").equals("None")) {
	          scheduleType = "None";
          } else if (request.getParameter("scheduleType").equals("Dynamic")) {
	          scheduleType = "Dynamic";
          } else {
        	  scheduleType = "Static";
          }
      }
      scheduleTimeslots = request.getParameter("scheduleTimeslots");
      eventInfoValues = request.getParameter("eventInfoValues");
      minValue = request.getParameter("minValue");
      maxValue = request.getParameter("maxValue");
      eventInfoType = new EventInfoType();
      eventInfoType.setName(typeName);
      eventInfoType.setTypeIDString(typeID);
      eventInfoType.setScheduleType(scheduleType);
      eventInfoType.setMinValue(Double.valueOf(minValue));
      eventInfoType.setMaxValue(Double.valueOf(maxValue));
	  eventInfoType.setProgramName((String) session.getAttribute("programName"));
	  
	  DataAccessObject.createObject(eventInfoType);
      
      if (scheduleTimeslots != null) {
	      String [] tokens = scheduleTimeslots.split(",");
	      // schedule time slot values need to be sorted into ascending order
	      java.util.Arrays.sort(tokens);
	      // Always need to have a 0 as the first element (see XSD)
	      if (!tokens[0].equals("0") && !tokens[0].equals("0.0")) {
	    	  ScheduleTimeSlot ts = new ScheduleTimeSlot();
	    	  ts.setEventInfoTypeName(eventInfoType.getName());
	    	  ts.setTimeSlot(new Double(0));
	    	  DataAccessObject.createObject(ts);
	      }
	      for (int i = 0; i < tokens.length; i++) {
              ScheduleTimeSlot ts = new ScheduleTimeSlot();
              ts.setEventInfoTypeName(eventInfoType.getName());
              ts.setTimeSlot(Double.parseDouble(tokens[i]));
              DataAccessObject.createObject(ts);
	      }
      }
      
      if (eventInfoValues != null) {
	      String [] tokens = eventInfoValues.split(",");
          for (int i = 0; i < tokens.length; i++) {
              EventInfoValue eiv = new EventInfoValue();
              eiv.setEventInfoTypeName(eventInfoType.getName());
              eiv.setValue(new Integer(Integer.parseInt(tokens[i])));
              eiv.setProgramName(eventInfoType.getProgramName());
              DataAccessObject.createObject(eiv);
	      }
      }
  } catch (Exception iex) {
	  System.out.println("Incorrect form input: " + iex);
	  iex.printStackTrace();
	  %>
	  <jsp:forward page="CreateEventInfoType.jsp"/>
	  <%
  }
  String buttonPressed = request.getParameter("submitEIT");
  if (buttonPressed != null && buttonPressed.equals("Add Another EventInfoType")) {
%>
      <jsp:forward page="CreateEventInfoType.jsp"/>
<%
  } else {
%>
      <jsp:forward page="CompleteProgramCreation.jsp"/>
<% 
  }
%>
</body>
</html>