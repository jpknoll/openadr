<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
  import = "java.io.*"
  import = "java.sql.*"
  import = "java.util.*"
  import = "javax.sql.*"
%>
<%
  Connection con = null;  
  ResultSet rs = null;
  Statement stmt = null;
  String username = request.getParameter("username");
  session.setAttribute("username", username);
  String password = request.getParameter("password");
%>
<html>
<body>
<%
  try {
    Class.forName("oracle.jdbc.driver.OracleDriver");
    con =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "o2adr", "o2adr");
    stmt = con.createStatement();
    rs = stmt.executeQuery("SELECT accountID FROM ParticipantAccount WHERE userName = '" + username +
        		           "' AND password = '" + password + "'");
    if (rs.next()) {
    	String accountID = rs.getString(1);
    	session.setAttribute("accountID", accountID);
    	%>
    	<jsp:forward page="CreateDRASClient.jsp"/>
    	<%
    } else {
    	System.out.println("Invalid login.");
    	%>
    	<jsp:forward page="CreateDRASClient.html"/>
    	<%
    }
  } catch (Exception e) {
      System.out.println("Error occurred " + e);
      %>
  	  <jsp:forward page="CreateDRASClient.html"/>
  	  <%
  } finally {
  	try {
	    if (stmt != null) stmt.close();
	    }  catch (SQLException e) {}
	try {
	    if (con != null) con.close();
	} catch (SQLException e) {}
}
%>
</body>
</html>