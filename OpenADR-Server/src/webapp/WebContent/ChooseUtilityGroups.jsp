<%@page import="java.sql.*"%>
<html>
<head>
<title>Choose Utility Groups to join:</title>
</head>
<body>
<%! String[] programs; 
    String createEntity;
    String nextFormAction;
%>
<% 
    programs = request.getParameterValues("programs");
    Connection conn = null;
    int result = -1;
    ResultSet queryResult = null;
    Statement stmt = null;
    ResultSetMetaData rsmd = null;
    createEntity = (String) session.getAttribute("createEntity");
    if (createEntity != null && createEntity.equals("ParticipantAccount")) {
    	nextFormAction = "CompleteParticipantRegistration.jsp";
    } else {
        nextFormAction = "CreateDRASClient.jsp";	
    }
    
    try {
	    Class c = Class.forName("oracle.jdbc.driver.OracleDriver");
    } catch (Exception e) {
	    System.out.println("Error occurred " + e);
	    e.printStackTrace();
	}
	try {
	    conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "o2adr", "o2adr");
	} catch (SQLException e) {
	    System.out.println("Error occurred " + e);
	    e.printStackTrace();
	}
	    
    if (programs != null && createEntity != null) 
    {
    	if (createEntity.equals("ParticipantAccount")) {
    		// this is the case where programs are being added for at a participant level
	    	String accountIdentifier = (String) session.getAttribute("accountID");
		    String name = (String) session.getAttribute("name");
	    	if (name != null) {
	    		stmt = conn.createStatement();
	        	result = stmt.executeUpdate("DELETE FROM ProgramParticipant WHERE accountID = '" +
	        								accountIdentifier + "'");
	    	}
        	for (int i = 0; i < programs.length; i++) 
        	{
            	try {
     	        	stmt = conn.createStatement();
     	        	result = stmt.executeUpdate("INSERT INTO ProgramParticipant (programParticipantId, " + 
     	        			                    "                                accountID, programName) "+
     	                	                    "VALUES(ProgramParticipant_seq.nextval, '" + 
     	                	                    accountIdentifier + "'" +
     	                    	                ", '" + programs[i] + "')");
     	    	} catch (SQLException e) {
     		        System.out.println("Error occurred " + e);
     		        e.printStackTrace();
     	    	}
        	}
    	} else { // this is the case where programs are being added for an individual DRAS client
	    	String drasClientId = (String) session.getAttribute("drasClientId");
		    String name = (String) session.getAttribute("name");
	    	if (name != null) {
	    		stmt = conn.createStatement();
	        	result = stmt.executeUpdate("DELETE FROM ProgramDRASClient WHERE drasClientId = '" +
	        								drasClientId + "'");
	    	}
        	for (int i = 0; i < programs.length; i++) 
        	{
            	try {
     	        	stmt = conn.createStatement();
     	        	result = stmt.executeUpdate("INSERT INTO ProgramDRASClient (programDrasClientId, " + 
     	        			                    "                               drasClientId, programName) "+
     	                	                    "VALUES(ProgramParticipant_seq.nextval, '" + 
     	                	                    drasClientId + "'" +
     	                    	                ", '" + programs[i] + "')");
     	    	} catch (SQLException e) {
     		        System.out.println("Error occurred " + e);
     		        System.out.println("Attempted to insert: drasClientId = " + drasClientId + " programName " +
     		        		           programs[i]);
     		        e.printStackTrace();
     	    	}
        	}    		
    	}
   }
   try {
       stmt = conn.createStatement();
       queryResult = stmt.executeQuery("SELECT utilityGroupId, name FROM UtilityGroup");
   } catch (SQLException e) {
       System.out.println("Error occurred " + e);
       e.printStackTrace();
   }

   int columns=0;
   try {
       rsmd = queryResult.getMetaData();
       columns = rsmd.getColumnCount();
   } catch (SQLException e) {
       System.out.println("Error occurred " + e);
       e.printStackTrace();
   }
%>
<FORM method="POST" ACTION="<%=nextFormAction%>">
<% 
if (nextFormAction.equals("CompleteParticipantRegistration.jsp")) {
%>
<h1>Choose Utility Groups to join:</h1>
<table width="90%" border="1">
  <tr>
  <% // write out the header cells containing the column labels
     try {
    	out.write("<th>Select:</th>");
        for (int i=1; i<=columns; i++) {
             out.write("<th>" + rsmd.getColumnLabel(i) + "</th>");
        }
  %>
  </tr>


  <% // now write out one row for each entry in the database table
        while (queryResult.next()) {
           out.write("<tr>");
           out.write("<td><input TYPE=checkbox name=groups VALUE=" +
        		     queryResult.getString(1) + "></td>");
           for (int i=1; i<=columns; i++) {
             out.write("<td>" + queryResult.getString(i) + "</td>");
           }
           out.write("</tr>");
        }

        // close the connection and the statement
        stmt.close();
        conn.close();
     } // end of the try block
     catch (SQLException e) {
        System.out.println("Error " + e);
        e.printStackTrace();
     }
     // ensure everything is closed
   finally {
    try {
      if (stmt != null)
       stmt.close();
      }  catch (SQLException e) {}
      try {
       if (conn != null)
        conn.close();
       } catch (SQLException e) {}
   }
   %>
</table>
<br> <INPUT TYPE=submit name=submit Value="Submit">
<% } else { %>
<h2>Mappings from this client to the selected programs have been persisted.</h2>
<INPUT TYPE=submit name=submit Value="Finish">
<% } %>
</FORM>
</body>
</html>