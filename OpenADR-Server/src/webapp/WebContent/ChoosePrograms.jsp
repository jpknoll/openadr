<%@page import="java.sql.*"%>
<html>
<head>
<title>Choose Programs to participate in:</title>
</head>
<body>
<FORM method="POST" ACTION="ChooseUtilityGroups.jsp">
<h1>Choose Programs to participate in:</h1>
<%
    Connection conn = null;
    ResultSet result = null;
    Statement stmt = null;
    ResultSetMetaData rsmd = null;

    try {
      Class c = Class.forName("oracle.jdbc.driver.OracleDriver");
    }
    catch (Exception e) {
      System.out.println("Error occurred " + e);
    }
     try {
       conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "o2adr", "o2adr");
     }
     catch (SQLException e) {
        System.out.println("Error occurred " + e);
     }
     try {
        stmt = conn.createStatement();
        result = stmt.executeQuery("SELECT name, priority FROM UtilityProgram");
     }
     catch (SQLException e) {
         System.out.println("Error occurred " + e);
      }

     int columns=0;
     try {
       rsmd = result.getMetaData();
       columns = rsmd.getColumnCount();
     }
     catch (SQLException e) {
        System.out.println("Error occurred " + e);
     }
%>
<table width="90%" border="1">
  <tr>
  <% // write out the header cells containing the column labels
     try {
    	out.write("<th>Select:</th>");
        for (int i=1; i<=columns; i++) {
             out.write("<th>" + rsmd.getColumnLabel(i) + "</th>");
        }
  %>
  </tr>


  <% // now write out one row for each entry in the database table
        while (result.next()) {
           out.write("<tr>");
           out.write("<td><input TYPE=checkbox name=programs VALUE=\"" +
        		     result.getString(1) + "\"></td>");
           for (int i=1; i<=columns; i++) {
             out.write("<td>" + result.getString(i) + "</td>");
           }
           out.write("</tr>");
        }

        // close the connection and the statement
        stmt.close();
        conn.close();
     } // end of the try block
     catch (SQLException e) {
        System.out.println("Error " + e);
     }
     // ensure everything is closed
   finally {
    try {
      if (stmt != null)
       stmt.close();
      }  catch (SQLException e) {}
      try {
       if (conn != null)
        conn.close();
       } catch (SQLException e) {}
   }

   %>
</table>
<br> <INPUT TYPE=submit name=submit Value="Submit">
</FORM>
</body>
</html>