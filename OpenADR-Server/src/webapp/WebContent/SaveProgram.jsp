<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
  import = "java.math.BigInteger"
  import = "org.openadr.dataobjects.program.*"
  import = "org.openadr.*"
%>
<%
  String name = request.getParameter("name");
  session.setAttribute("programName", name);
  String priority = request.getParameter("priority");
%>
<html>
<head>
  <title>Inserting Program</title>
</head>
<body>
<%
  try {
      UtilityProgram program = new UtilityProgram();
      program.setName(name);
      Integer pri = new Integer(priority);
      program.setPriority(pri);
      DataAccessObject.createObject(program);
      session.setAttribute("programName", program.getName());
  } catch (Exception ex) {
	  System.out.println("Incorrect form input: " + ex);
	  %>
	  <jsp:forward page="CreateProgram.html"/>
	  <%
  }
  String buttonPressed = request.getParameter("submit");
  if (buttonPressed != null && buttonPressed.equals("Add EventInfoType")) {
%>
      <jsp:forward page="CreateEventInfoType.jsp"/>
<%
  } else {
%>
      <jsp:forward page="CompleteProgramCreation.jsp"/>
<% 
  }
%>
</body>
</html>