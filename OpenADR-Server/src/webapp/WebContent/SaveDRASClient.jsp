<%@ page language = "java" contentType = "text/html; charset = ISO-8859-1"
  import = "java.io.*"
  import = "java.util.*"
  import = "java.math.BigInteger"
  import = "org.openadr.*"
  import = "org.openadr.participant.*"
  import = "org.openadr.dataobjects.participant.*"
  import = "org.openadr.dataobjects.location.*"
  import = "org.openadr.dataobjects.logging.*"
  import = "org.openadr.dataobjects.client.*"
  
%>
<%
  String accountID = (String) session.getAttribute("accountID");
  String clientType = "SMART";
  String onLineStatus = "0";
  String connectionType = "PULL";
  String clientURI = null;
  String clientAuthentication= null;
  int pollingPeriod = 0;
  int retryCount = 0;
  int retryPeriod = 0;
  String gridLocation = null;
  double latitude;
  double longitude;
  String address = null;
  String identifier = null;
  
  try {
      if (request.getParameter("onLineStatus") != null) {
	      if (request.getParameter("onLineStatus").equals("Online")) {
		      onLineStatus = "1";
	      } else {
		      onLineStatus = "0";
	      }
      }
      
      identifier = request.getParameter("identifier");
      gridLocation = request.getParameter("gridLocation");
      latitude = Double.parseDouble(request.getParameter("latitude"));
      longitude = Double.parseDouble(request.getParameter("longitude"));
      address = request.getParameter("address");
      
      Location location = new Location();
      DRASClient client = new DRASClient();
      
      client.setIdentifier(identifier);
      client.setParticipantID(accountID);
      
      location.setGridLocation(gridLocation);
      location.setLatitude(latitude);
      location.setLongitude(longitude);
      location.setAddress(address);
      
      DataAccessObject.createObject(location);
      
      client.setLocationId(location.getLocationId());
      client.setClientAuthentication(clientAuthentication);
      client.setClientURI(clientURI);
      client.setConnectionType(connectionType);
      client.setPollingPeriod(BigInteger.valueOf(pollingPeriod));
      client.setRetryCount(new Long(retryCount));
      client.setRetryPeriod(BigInteger.valueOf(retryPeriod));
      
      client.setClientType(clientType);
      if (onLineStatus.equals("0"))
	      client.setOnLine(false);
      else
    	  client.setOnLine(true);
      
      DataAccessObject.createObject(client);
      
      session.setAttribute("drasClientId", client.getIdentifier());
      
      // log the client creation
      TransactionLog tLog = new TransactionLog();
      tLog.setDescription("New DRAS client " + client.getIdentifier() + " created for account " + accountID);
      tLog.setResult("SUCCESS");
      tLog.setRole("Program Operator");
      tLog.setTimeStamp(new java.util.Date(System.currentTimeMillis()));
      tLog.setTransactionName("CreateDRASClient");
      tLog.setUserName((String) session.getAttribute("username"));
      DataAccessObject.createObject(tLog);
      session.setAttribute("createEntity", "DRASClient");
  } catch (Exception ex) {
	  System.out.println("Problem inserting records to the DB: " + ex);
	  ex.printStackTrace();
	  %>
	  <jsp:forward page="CreateDRASClient.jsp"/>
	  <%
  }
%>
<html>
<head>
  <title>Inserting DRAS Client</title>
</head>
<body>
<jsp:forward page="ChoosePrograms.jsp"/>
</body>
</html>