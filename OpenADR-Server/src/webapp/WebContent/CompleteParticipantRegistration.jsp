<%@page import="java.sql.*"%>
<html>
<head><title>Participant Account created.</title></head>
<body>
<h1>Participant Account created.</h1>
<%! String[] groups; %>
<% 
    groups = request.getParameterValues("groups");
    Connection conn = null;
    int result = -1;
    Statement stmt = null;
    ResultSetMetaData rsmd = null;
    
    try {
	    Class c = Class.forName("oracle.jdbc.driver.OracleDriver");
	} catch (Exception e) {
	    System.out.println("Error occurred " + e);
	}
	try {
	    conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "o2adr", "o2adr");
	} catch (SQLException e) {
	    System.out.println("Error occurred " + e);
	}

    if (groups != null) 
    {
    	String accountID = (String) session.getAttribute("accountID");
	    String name = (String) session.getAttribute("name");
	    if (name != null) {
	    	stmt = conn.createStatement();
	        result = stmt.executeUpdate("DELETE FROM GroupMembership WHERE accountID = '" +
                                        accountID + "'");
	    }
        for (int i = 0; i < groups.length; i++) 
        {
            try {
     	        stmt = conn.createStatement();
     	        result = stmt.executeUpdate("INSERT INTO GroupMembership (groupMembershipId, " + 
     	        		                    "                             accountID, utilityGroupId) "+
     	                                    "VALUES(GroupMembership_seq.nextval, '" + 
     	                                    accountID + "'" +
     	                                    ", '" + groups[i] + "')");
     	    }
     	    catch (SQLException e) {
     	        System.out.println("Error occurred " + e);
     	    }
      }
   }
   try {
       if (stmt != null)
           stmt.close();
   } catch (SQLException e) {}
   try {
       if (conn != null)
           conn.close();
   } catch (SQLException e) {}
%>
<FORM method="POST" ACTION="CreateParticipantAccount.html">
<INPUT TYPE=submit name=submit Value="Finish">
</FORM>
</body>
</html>