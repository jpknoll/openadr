/**
 * 
 */
package org.openadr.hibernate;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class EventStateHomeTest.
 *
 * @author jpknoll
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-hibernate.xml")
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class EventStateHomeTest {

	/** The target. */
	@Autowired
	private EventStateHome target;
	
	private static final Log log = LogFactory.getLog(EventStateHomeTest.class);
	
	/**
	 * Sets the up before class.
	 *
	 * @throws Exception the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * Tear down after class.
	 *
	 * @throws Exception the exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link org.openadr.hibernate.EventStateHome#persist(org.openadr.hibernate.EventState)}.
	 */
	@Test
	@Transactional
	public void testPersist() {
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.openadr.hibernate.EventStateHome#attachDirty(org.openadr.hibernate.EventState)}.
	 */
	@Test
	@Transactional
	public void testAttachDirty() {
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.openadr.hibernate.EventStateHome#attachClean(org.openadr.hibernate.EventState)}.
	 */
	@Test
	@Transactional
	public void testAttachClean() {
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.openadr.hibernate.EventStateHome#delete(org.openadr.hibernate.EventState)}.
	 */
	@Test
	@Transactional
	public void testDelete() {
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.openadr.hibernate.EventStateHome#merge(org.openadr.hibernate.EventState)}.
	 */
	@Test
	@Transactional
	public void testMerge() {
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.openadr.hibernate.EventStateHome#findById(long)}.
	 */
	@Test
	@Transactional
	public void testFindById() {
		
		log.debug("");
		EventState state = target.findById(1);
		assertNotNull(state);
	}

	/**
	 * Test method for {@link org.openadr.hibernate.EventStateHome#findByExample(org.openadr.hibernate.EventState)}.
	 */
	@Test
	@Transactional
	public void testFindByExample() {
		EventState example = new EventState();
		example.setEventStateId(1);
		
		List states = target.findByExample(example);
		//assertTrue(states.size() > 0);
		
		//fails
	}
	
	/**
	 * Test method for {@link org.openadr.hibernate.EventStateHome#findByDrasClientIdentifier(String)} 
	 */
	@Test
	@Transactional
	public void testFindByDrasClientIdentifier() {
		String drasClientIdentifier = "4";
		
		List states = target.findByDrasClientIdentifier(drasClientIdentifier);
		assertTrue(states.size() > 0);
	}
}
