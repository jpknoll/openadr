package org.openadr.config;

public class Constants {

	public static final String drasName = "OpenSource - OpenAdr";
	
	static public class Services {

		public static String PROGRAM = "PROGRAM_SERVICE";
		public static String EVENT = "EVENT_SERVICE";
		public static String EVENTSTATE = "EVENTSTATE_SERVICE";
		public static String FEEDBACK = "FEEDBACK_SERVICE";
	}
	
	static public class Transactions {

		public static String GETPROGRAM = "Get Program";
		public static String UPDATEPROGRAM = "Update Program";
		public static String DELETEPROGRAM = "Delete Program";
		public static String CREATEPROGRAM = "Create Program";
		

		public static String GETEVENT = "Get Event";
		public static String UPDATEEVENT = "Update Event";
		public static String DELETEEVENT = "Delete Event";
		public static String CREATEEVENT = "Create Event";

		public static String GETEVENTSTATE = "Get Event State";
		public static String CREATECONFIRMATION = "Create Event State Confirmation";
		
		public static String GETFEEDBACK = "Get Feedback";
		public static String CREATECFEEDBACK = "Create Feedback";

	}
}
