/* 
**** Copyright Notice ***
* Open Source OpenADR Toolkit, Copyright (c) 2011, 
* The Regents of the University of California, through Lawrence Berkeley National 
* Laboratory (subject to receipt of any required approvals from the U.S. Dept. 
* of Energy).  All rights reserved.
*
* Portions of this software package were developed for Ernest Orlando Lawrence 
* Berkeley National Laboratory by Utility Integrated Solutions, Inc., 224 Benthill 
* Ct. Lafayette, CA, 94549,  (925) 939-0449
*
* If you have questions about your rights to use or distribute this software, 
* please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.
*/

package org.openadr.properties;

import java.io.IOException;
import java.util.Properties;

public class ConfigurationManager extends Properties {

	private String programUrl = "http://localhost:9000/OpenAdr/ProgramService/programs/";
	private String eventUrl = "http://localhost:9000/OpenAdr/EventService/events/";
	private String eventStateUrl = "http://localhost:9000/OpenAdr/EventStateService/eventstates";
	
	public ConfigurationManager() {		
		try {
			this.load(this.getClass().getResourceAsStream("client.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getProgramGetUrl() {
		return processUrl(this.getProperty("programget", programUrl));
	}	
	public String getProgramAddUrl() {
		return processUrl(this.getProperty("programadd", programUrl));
	}
	public String getProgramUpdUrl() {
		return processUrl(this.getProperty("programupd", programUrl));
	}
	public String getProgramDelUrl() {
		return processUrl(this.getProperty("programdel", programUrl));
	}

	public String getEventGetUrl() {
		return processUrl(this.getProperty("eventget", eventUrl));
	}
	public String getEventAddUrl() {
		return processUrl(this.getProperty("eventadd", eventUrl));
	}
	public String getEventUpdUrl() {
		return processUrl(this.getProperty("eventupd", eventUrl));
	}
	public String getEventDelUrl() {
		return processUrl(this.getProperty("eventdel", eventUrl));
	}

	public String getEventStateGetUrl() {
		return processUrl(this.getProperty("eventstateget", eventStateUrl));
	}

	public String getEventStateAddUrl() {
		return processUrl(this.getProperty("eventstateadd", eventStateUrl));
	}

	private String processUrl(String url) {
		String hosturl = getProperty("hosturl", "localhost:8080");
		return url.replaceAll("hosturl", hosturl);
	}
}

