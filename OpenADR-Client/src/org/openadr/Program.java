package org.openadr;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import javax.xml.ws.Holder;

import org.openadr.dras.drasclientsoap.DRASClientSOAPPortType;
import org.openadr.dras.drasclientsoap.DRASClientSOAPService;
import org.openadr.dras.drasclientsoap.ListOfEventStates;

public class Program {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			//Program prg = new Program();
			//prg.printClassLoader(prg.getClass().getClassLoader());
		
			//URL url = new URL("http://127.0.0.1:8080/OpenADR-Service/services/drasclientsoap?wsdl");
			DRASClientSOAPService service = new DRASClientSOAPService();			
			DRASClientSOAPPortType port = service.getDRASClientSOAPPort();
			
			//fetch list of event status's
			Object empty = null;
			Holder<String> returnValue = new Holder<String>();
			Holder<ListOfEventStates> eventStates = new Holder<ListOfEventStates>();
			port.getEventStates(empty, returnValue, eventStates);
			
			System.out.println(returnValue.value);
			
			return;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void printClassLoader(ClassLoader classLoader) {
	    if (null == classLoader) {
	        return;
	    }
	    System.out.println("--------------------");
	    System.out.println(classLoader);
	    if (classLoader instanceof URLClassLoader) {
	        URLClassLoader ucl = (URLClassLoader) classLoader;
	        int i = 0;
	        for (URL url : ucl.getURLs()) {
	            System.out.println("url[" + (i++) + "]=" + url);
	        }
	    }
	    printClassLoader(classLoader.getParent());
	}

}
