/**
 * 
 */
package org.openadr.dto.mapping.converters;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import ma.glasnost.orika.metadata.TypeFactory;

import org.junit.AfterClass;
import org.junit.Test;
import org.openadr.dras.mapping.converters.BigDecimalToLongConverter;

/**
 * @author jpknoll
 *
 */
public class BigDecimalToLongConverterTest {

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Test method for {@link org.openadr.dras.mapping.converters.BigDecimalToLongConverter#convertTo(java.math.BigDecimal, ma.glasnost.orika.metadata.Type)}.
	 */
	@Test
	public void testConvertToBigDecimalTypeOfLong() {
		
		BigDecimal source = new BigDecimal(42);
		BigDecimalToLongConverter converter = new BigDecimalToLongConverter();
		Long destination = converter.convertTo(source, TypeFactory.valueOf(Long.class));
		
		assertTrue(new Long(42).compareTo(destination) == 0);
	}

	/**
	 * Test method for {@link org.openadr.dras.mapping.converters.BigDecimalToLongConverter#convertFrom(java.lang.Long, ma.glasnost.orika.metadata.Type)}.
	 */
	@Test
	public void testConvertFromLongTypeOfBigDecimal() {
		
		Long source = new Long(764);
		BigDecimalToLongConverter converter = new BigDecimalToLongConverter();
		BigDecimal destination = converter.convertFrom(source, TypeFactory.valueOf(BigDecimal.class));
		
		assertTrue(new BigDecimal(764).compareTo(destination) == 0);
	}

}
