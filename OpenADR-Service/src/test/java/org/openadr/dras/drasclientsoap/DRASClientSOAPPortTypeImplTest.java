/**
 * 
 */
package org.openadr.dras.drasclientsoap;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.ws.security.WSUsernameTokenPrincipal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 * @author jpknoll
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext-hibernate.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class DRASClientSOAPPortTypeImplTest {

	@Autowired
	private DRASClientSOAPPortTypeImpl target;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link org.openadr.dras.drasclientsoap.DRASClientSOAPPortTypeImpl#setEventStateConfirmation(org.openadr.dras.eventstate.EventStateConfirmation)}.
	 */
	@Test
	public void testSetEventStateConfirmation() {
		//mock request object
		WSUsernameTokenPrincipal mockedWsutp = mock(WSUsernameTokenPrincipal.class);
		when(mockedWsutp.getName()).thenReturn("ws-client");
		
		//fail("Not yet implemented");
	}

	/**
	 * Test method for {@link org.openadr.dras.drasclientsoap.DRASClientSOAPPortTypeImpl#getEventStates(java.lang.Object, javax.xml.ws.Holder, javax.xml.ws.Holder)}.
	 */
	@Test
	public void testGetEventStates() {
		//fail("Not yet implemented");
	}

}
