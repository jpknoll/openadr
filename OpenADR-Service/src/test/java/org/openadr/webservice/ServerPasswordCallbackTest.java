/**
 * 
 */
package org.openadr.webservice;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author jpknoll
 *
 */
public class ServerPasswordCallbackTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link org.openadr.webservice.ServerPasswordCallback#handle(javax.security.auth.callback.Callback[])}.
	 */
	@Test
	public void testHandle() {
		try {
		
			//mock ws-security callback
			WSPasswordCallback wsCallback = mock(WSPasswordCallback.class);
			when(wsCallback.getIdentifier()).thenReturn("ws-client");
			when(wsCallback.getPassword()).thenReturn("password");
					
			//setup
			Callback[] callbacks = new Callback[] { wsCallback };
			ServerPasswordCallback target = new ServerPasswordCallback();
		
			target.handle(callbacks);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedCallbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
