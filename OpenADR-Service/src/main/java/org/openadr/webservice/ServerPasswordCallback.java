/**
 * 
 */
package org.openadr.webservice;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ws.security.WSPasswordCallback;
import org.openadr.hibernate.EventStateHome;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author jpknoll
 *
 */
public class ServerPasswordCallback implements CallbackHandler {

	@Autowired
    private static final Log log = LogFactory.getLog(ServerPasswordCallback.class);
	
	/**
	 * 
	 */
	public ServerPasswordCallback() {

	}

	/* (non-Javadoc)
	 * @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[])
	 */
	@Override
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		log.debug("identifier: " + pc.getIdentifier());
		
		if (pc.getIdentifier().equals("ws-client")) {
			// set the password on the callback. This will later be compared to the
			// password which was sent from the client.
			pc.setPassword("password");
		}
	}
}
