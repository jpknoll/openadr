package org.openadr.dras.drasbacnet;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import services.web.bacnet.ArrayOfstring;
import services.web.bacnet.BacnetPortType;

@WebService(
		targetNamespace = "http://www.openadr.org/DRAS/DRASClientSOAP",
		portName="BacnetPortType",
		serviceName="BacnetPortTypeService",
		endpointInterface = "services.web.bacnet.BacnetPortType",
		wsdlLocation= "WEB-INF/wsdl/DRASClientBACnet.wsdl")
@SOAPBinding(style=Style.DOCUMENT, use=Use.LITERAL, parameterStyle=ParameterStyle.WRAPPED)
public class BacnetPortTypeImpl implements BacnetPortType {

	public BacnetPortTypeImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	@WebResult(name = "results", targetNamespace = "urn:bacnet-web-services", partName = "results")
	@WebMethod
	public ArrayOfstring getSupportedLocals(
			@WebParam(partName = "options", name = "options") String options) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@WebResult(name = "result", targetNamespace = "urn:bacnet-web-services", partName = "result")
	@WebMethod
	public String getValue(
			@WebParam(partName = "options", name = "options") String options,
			@WebParam(partName = "path", name = "path") String path) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@WebResult(name = "result", targetNamespace = "urn:bacnet-web-services", partName = "result")
	@WebMethod
	public String getDefaultLocale(
			@WebParam(partName = "options", name = "options") String options) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@WebResult(name = "result", targetNamespace = "urn:bacnet-web-services", partName = "result")
	@WebMethod
	public String setValue(
			@WebParam(partName = "options", name = "options") String options,
			@WebParam(partName = "path", name = "path") String path,
			@WebParam(partName = "value", name = "value") String value) {
		// TODO Auto-generated method stub
		return null;
	}

}
