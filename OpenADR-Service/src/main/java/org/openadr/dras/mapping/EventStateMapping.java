package org.openadr.dras.mapping;

import org.openadr.dras.eventstate.EventState;

import ma.glasnost.orika.*;
import ma.glasnost.orika.metadata.TypeFactory;

public class EventStateMapping {
	
	public static void configure(MapperFactory factory) {

		factory.registerObjectFactory(new EventStateFactory(), TypeFactory.valueOf(org.openadr.dras.eventstate.EventState.class));
		
		factory.registerClassMap(
			factory.classMap(org.openadr.hibernate.EventState.class, org.openadr.dras.eventstate.EventState.class)
			.fieldAToB("utilityDREvent.utilityProgram.name", "programName")
			.fieldAToB("utilityDREvent.eventIdentifier", "eventIdentifier")
			.fieldAToB("drasClient.identifier", "drasClientID")
			.field("eventStateId", "eventStateID")
			.field("offlineStatus", "offLine")
			.byDefault(	)
			.toClassMap());
	}
	
	public static class EventStateFactory implements ObjectFactory<org.openadr.dras.eventstate.EventState> {

		@Override
		public EventState create(Object source, MappingContext mappingContext) {
			org.openadr.dras.eventstate.EventState state = new org.openadr.dras.eventstate.EventState();
			state.setSchemaVersion("20080509");
			
			return state;
		}
		
	}

}
