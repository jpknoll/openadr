package org.openadr.dras.mapping;

import org.openadr.dras.eventstate.EventStateConfirmation;

import ma.glasnost.orika.*;
import ma.glasnost.orika.metadata.TypeFactory;

public class EventStateConfirmationMapping  {

	public static void configure(MapperFactory factory) {

		factory.registerObjectFactory(new EventStateConfirmationFactory(), TypeFactory.valueOf(org.openadr.dras.eventstate.EventStateConfirmation.class));
		
		factory.registerClassMap(
			factory.classMap(org.openadr.hibernate.EventStateConfirmation.class, org.openadr.dras.eventstate.EventStateConfirmation.class)
		    .fieldAToB("eventState.utilityDREvent.utilityProgram.name","programName") 
	    	.fieldAToB("eventState.utilityDREvent.eventIdentifier","eventIdentifier")
		    .fieldAToB("eventState.drasClient.identifier","drasClientID")
		 	.fieldAToB("eventState.eventStateId","eventStateID")
		 	.byDefault()
			.toClassMap());
		
	}
	
	public static class EventStateConfirmationFactory implements ObjectFactory<org.openadr.dras.eventstate.EventStateConfirmation> {

		@Override
		public EventStateConfirmation create(Object source, MappingContext mappingContext) {

			org.openadr.dras.eventstate.EventStateConfirmation state = new org.openadr.dras.eventstate.EventStateConfirmation();
			state.setSchemaVersion("20080509");
			
			return state;
		}
		
	}

}
