package org.openadr.dras.mapping;

import org.openadr.dras.mapping.converters.*;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class ConfigurableMapperImpl extends ConfigurableMapper{

	public ConfigurableMapperImpl() {
		super();
	}

	@Override
	protected void configure(MapperFactory factory) {
		
		factory.getConverterFactory().registerConverter(new BigDecimalToLongConverter());
		
		EventStateMapping.configure(factory);
		EventStateConfirmationMapping.configure(factory);
	}
	
	@Override
	protected void configureFactoryBuilder(DefaultMapperFactory.Builder factoryBuilder) {
        
    }
}
