package org.openadr.dras.mapping;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.ObjectFactory;
import ma.glasnost.orika.metadata.Type;

/**
 * The Class EventStateConfirmationConverter.
 */
public class EventStateConfirmationDAOConverter
		extends
		CustomConverter<org.openadr.dras.eventstate.EventStateConfirmation, org.openadr.hibernate.EventStateConfirmation> {

	/**
	 * Instantiates a new event state confirmation converter.
	 */
	public EventStateConfirmationDAOConverter() {
	}

	@Override
	public org.openadr.hibernate.EventStateConfirmation convert(
			org.openadr.dras.eventstate.EventStateConfirmation source,
			Type<? extends org.openadr.hibernate.EventStateConfirmation> destinationType) {

		return null;
	}

	
	
	/**
	 * A factory for creating org.openadr.dras.eventstate.EventStateConfirmation
	 * objects.
	 */
	public static class EventStateConfirmationDTOFactory implements
			ObjectFactory<org.openadr.dras.eventstate.EventStateConfirmation> {

		@Override
		public org.openadr.dras.eventstate.EventStateConfirmation create(
				Object source, MappingContext mappingContext) {

			org.openadr.dras.eventstate.EventStateConfirmation state = new org.openadr.dras.eventstate.EventStateConfirmation();
			state.setSchemaVersion("20080509");

			return state;
		}

	}

	/**
	 * A factory for creating EventStateConfirmationDAO objects.
	 */
	public static class EventStateConfirmationDAOFactory implements
			ObjectFactory<org.openadr.hibernate.EventStateConfirmation> {

		@Override
		public org.openadr.hibernate.EventStateConfirmation create(
				Object source, MappingContext mappingContext) {

			return new org.openadr.hibernate.EventStateConfirmation();
		}

	}

}
