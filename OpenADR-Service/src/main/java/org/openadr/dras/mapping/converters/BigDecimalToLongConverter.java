package org.openadr.dras.mapping.converters;

import java.math.BigDecimal;

import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class BigDecimalToLongConverter extends
	BidirectionalConverter<BigDecimal, Long> {
	
	/* (non-Javadoc)
	* @see ma.glasnost.orika.converter.BidirectionalConverter#convertTo(java.lang.Object, ma.glasnost.orika.metadata.Type)
	*/
	@Override
	public Long convertTo(BigDecimal source, Type<Long> destinationType) {
		return source.longValue();
	}
	
	/* (non-Javadoc)
	* @see ma.glasnost.orika.converter.BidirectionalConverter#convertFrom(java.lang.Object, ma.glasnost.orika.metadata.Type)
	*/
	@Override
	public BigDecimal convertFrom(Long source,
		Type<BigDecimal> destinationType) {
		return BigDecimal.valueOf(source.doubleValue());
	}
}
