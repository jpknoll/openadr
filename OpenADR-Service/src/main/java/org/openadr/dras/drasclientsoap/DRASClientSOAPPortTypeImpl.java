/**
 * 
 */
package org.openadr.dras.drasclientsoap;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceContext;

import ma.glasnost.orika.MapperFacade;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ws.security.WSUsernameTokenPrincipal;

import org.openadr.dras.eventstate.EventState;
import org.openadr.dras.eventstate.ListOfEventStates;
import org.openadr.dras.eventstate.EventStateConfirmation;
import org.openadr.hibernate.EventStateConfirmationHome;
import org.openadr.hibernate.EventStateHome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jpknoll
 *
 */

@WebService(
		targetNamespace = "http://www.openadr.org/DRAS/DRASClientSOAP",
		portName="DRASClientSOAPPort",
		serviceName="DRASClientSOAPService",
		endpointInterface = "org.openadr.dras.drasclientsoap.DRASClientSOAPPortType",
		wsdlLocation= "WEB-INF/wsdl/DRASClientSOAP.wsdl")
@SOAPBinding(style=Style.DOCUMENT, use=Use.LITERAL, parameterStyle=ParameterStyle.WRAPPED)
public class DRASClientSOAPPortTypeImpl implements DRASClientSOAPPortType {

	@Resource
	protected WebServiceContext wsContext;
	
	private static final String PRINCIPAL_RESULT = "wss4j.principal.result";
	
    private static final Log log = LogFactory.getLog(DRASClientSOAPPortTypeImpl.class);
	
	@Autowired
	private MapperFacade mapper;
	
	@Autowired
	private EventStateHome eventStateHome;
	
	@Autowired
	private EventStateConfirmationHome eventStateConfirmationHome;
	
	@Transactional
	public String setEventStateConfirmation(EventStateConfirmation eventStateConfirmation) {
		try
		{
			log.debug("setting eventStateConfirmation");
			
			log.debug("fetching parent dao");
			BigInteger eventStateId = eventStateConfirmation.getEventStateID();
			org.openadr.hibernate.EventState eventStateDAO = 
					eventStateHome.findById(eventStateId.longValue());
			
			log.debug("mapping to dao");
			org.openadr.hibernate.EventStateConfirmation eventStateConfirmationDAO = 
					mapper.map(eventStateConfirmation, org.openadr.hibernate.EventStateConfirmation.class);
			
			log.debug("attaching dao");
			eventStateConfirmationDAO.setEventState(eventStateDAO);
			eventStateConfirmationHome.attachDirty(eventStateConfirmationDAO);
			
			log.debug("set successful");
			return "SUCCESS";
		}
		catch(Exception ex)
		{
			log.error("set failed", ex);
			return "FAILURE";
		}	
	}

	@Transactional
	public void getEventStates(
			Object empty,
			Holder<String> returnValue,
			Holder<ListOfEventStates> eventStates) {	
		try
		{
			if (wsContext == null) {
				log.debug("web context null");
				returnValue.value = "fAILURE";
				return;
			}
			
			log.debug("parse user token");
			WSUsernameTokenPrincipal wsutp = (WSUsernameTokenPrincipal) wsContext.getMessageContext().get(PRINCIPAL_RESULT);
			
			if (wsutp == null) {
				log.debug("usertoken null");
				returnValue.value = "FAILURE";
				return;
			} 
			
			log.debug("fetch dao");
			List statesDAO = eventStateHome.findByDrasClientIdentifier(wsutp.getName());
			
			log.debug("map to dto");
			List states = mapper.mapAsList(statesDAO, EventState.class); 
						
			log.debug("map to dto list object");
			eventStates.value = new ListOfEventStates();
			for (Object state : states) {
				eventStates.value.getEventState().add((EventState) state);
			}
			
			log.debug("fetch successful with:" + eventStates.value.getEventState().size() + " returned");
			returnValue.value = "SUCCESS";

		}
		catch(Exception ex)
		{
			log.error("set failed", ex);
			returnValue.value = "FAILURE";
		}		
	}
}
