/**
 * 
 */
package org.openadr.dras.drasclientrest;

import java.math.BigInteger;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import ma.glasnost.orika.MapperFacade;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openadr.dras.drasclientsoap.DRASClientSOAPPortTypeImpl;
import org.openadr.dras.eventstate.EventState;
import org.openadr.dras.eventstate.EventStateConfirmation;
import org.openadr.dras.eventstate.ListOfEventStates;
import org.openadr.hibernate.EventStateConfirmationHome;
import org.openadr.hibernate.EventStateHome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jpknoll
 *
 */
@Path("/")

public class DRASClientRESTServiceImpl implements DRASClientRESTService {
	
    private static final Log log = LogFactory.getLog(DRASClientSOAPPortTypeImpl.class);
	
	@Autowired
	private MapperFacade mapper;
	
	@Autowired
	private EventStateHome eventStateHome;
	
	@Autowired
	private EventStateConfirmationHome eventStateConfirmationHome;
	
	/**
	 * 
	 */
	public DRASClientRESTServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	@GET
	@Path("/eventstate/")
	@Produces("application/xml")
	@Transactional
	public ListOfEventStates getEventStates(@PathParam("clientId") String clientId) {
		try
		{					
			log.debug("fetch dao");
			List statesDAO = eventStateHome.findByDrasClientIdentifier(clientId);
			
			log.debug("map to dto");
			List states = mapper.mapAsList(statesDAO, EventState.class); 
						
			log.debug("map to dto list object");
			ListOfEventStates eventStates = new ListOfEventStates();
			for (Object state : states) {
				eventStates.getEventState().add((EventState) state);
			}
			
			log.debug("fetch successful with:" + eventStates.getEventState().size() + " returned");
			return eventStates;
		}
		catch(Exception ex)
		{
			log.error("set failed", ex);
			return null;
		}		
		
	}

	@Override
	@POST
	@Path("/eventstate/{eventstateid}/")
	@Transactional
	public Response addEventStateConfirmation(EventStateConfirmation eventStateConfirmation) {
		ResponseBuilder builder = null;
		try
		{			
			log.debug("setting eventStateConfirmation");
			
			log.debug("fetching parent dao");
			BigInteger eventStateId = eventStateConfirmation.getEventStateID();
			org.openadr.hibernate.EventState eventStateDAO = 
					eventStateHome.findById(eventStateId.longValue());
			
			log.debug("mapping to dao");
			org.openadr.hibernate.EventStateConfirmation eventStateConfirmationDAO = 
					mapper.map(eventStateConfirmation, org.openadr.hibernate.EventStateConfirmation.class);
			
			log.debug("attaching dao");
			eventStateConfirmationDAO.setEventState(eventStateDAO);
			eventStateConfirmationHome.attachDirty(eventStateConfirmationDAO);
			
			log.debug("set successful");
			builder = Response.status(Status.CREATED);
		}
		catch(Exception ex)
		{
			log.error("set failed", ex);
			builder = Response.status(Status.INTERNAL_SERVER_ERROR);
		}	
		
		return builder.build();
	}

}
