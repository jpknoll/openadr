/**
 * 
 */
package org.openadr.dras.drasclientrest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.openadr.dras.eventstate.EventStateConfirmation;
import org.openadr.dras.eventstate.ListOfEventStates;

/**
 * @author jpknoll
 *
 */
public interface DRASClientRESTService {
	@GET
	@Path("/eventstate/")
	@Produces ("application/xml")
	public abstract ListOfEventStates getEventStates(@PathParam("clientId") String clientId);

	@POST
	@Path("/eventstate/{eventstateid}/")
	public abstract Response addEventStateConfirmation(EventStateConfirmation confirmation);
}
